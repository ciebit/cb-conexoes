<?php
/*
==================================
------------- CIEBIT -------------
==================================
*/

$msgs = [
    'erro' => [
        'texto' => 'Ocorreu um erro na conexão com o banco de dados',
        'tipo' => 'erro'
    ],
    'sucesso' => [
        'texto' => 'Conexão ao banco de dados estabelecida com sucesso',
        'tipo' => 'sucesso'
    ],
];
