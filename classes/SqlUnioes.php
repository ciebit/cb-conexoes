<?php

namespace Ciebit\Conexoes;

trait SqlUnioes
{
    private $unioes;

    private function gerarUnioes():string
    {
        if (! $this->unioes) {
            return '';
        }

        return implode(' ', $this->unioes);
    }
}
