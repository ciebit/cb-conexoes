<?php

namespace Ciebit\Conexoes;

trait SqlLimite
{
    private $limite;

    /**
     * Configura o limite de linhas obtidas
     */
    public function configLimite(int $ini = 0, int $fim = 30):self
    {
        $this->limite = [$ini, $fim];
        return $this;
    }

    /**
     * Gera o trecho de limite de um comando SQL
     */
    public function gerarLimite()
    {
        // Verificando filtro por quantidade
        if (! $this->limite) {
            return '';
        }

        return "LIMIT {$this->limite[0]}, {$this->limite[1]} ";
    }
}
