<?php

namespace Ciebit\Conexoes\Configuracoes\Persistencias;

use Ciebit\Conexoes\Configuracoes\Configuracao;
use Ciebit\Conexoes\Configuracoes\Conversores\DeArray as Conversor;

use function is_file;
use function dirname;
use function parse_ini_file;

class ArquivoIni
{
    private const ARQUIVO_NOME = 'cbconfig-conexoes.ini';

    private function buscar(): array
    {
        $caminho = getcwd();
        $arquivo = "{$caminho}/". self::ARQUIVO_NOME;

        while (! is_file($arquivo)) {
            $pastaPai = dirname($caminho, 1);

            if ($caminho == $pastaPai) {
                return [];
            }

            $caminho = $pastaPai;
            $arquivo = "{$caminho}/". self::ARQUIVO_NOME;
        }

        $configuracoes = parse_ini_file($arquivo, true);

        return $configuracoes ?: [];
    }

    public function obter(): Configuracao
    {
        $configuracoes = $this->buscar();
        return (new Conversor)->converter($configuracoes);
    }
}
