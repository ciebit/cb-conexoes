<?php

namespace Ciebit\Conexoes\Configuracoes\Conversores;

use Ciebit\Conexoes\Configuracoes\Configuracao;

use function filter_var;

use const FILTER_SANITIZE_STRING;

class DeArray
{
    public function converter(array $dados): Configuracao
    {
        $padrao = $this->padronizar($dados);

        return (new Configuracao)
        ->definirBanco($padrao['banco'])
        ->definirSenha($padrao['senha'])
        ->definirServidor($padrao['servidor'])
        ->definirTipo($padrao['tipo'])
        ->definirUsuario($padrao['usuario'])
        ;
    }

    private function padronizar(array $dados): array
    {
        $opcoes_texto = ['default' => ''];

        return [
            'banco' => filter_var($dados['banco'] ?? '', FILTER_SANITIZE_STRING, $opcoes_texto),
            'senha' => filter_var($dados['senha'] ?? '', FILTER_SANITIZE_STRING, $opcoes_texto),
            'servidor' => filter_var($dados['servidor'] ?? '', FILTER_SANITIZE_STRING, $opcoes_texto),
            'tipo' => filter_var($dados['tipo'] ?? '', FILTER_SANITIZE_STRING, $opcoes_texto),
            'usuario' => filter_var($dados['usuario'] ?? '', FILTER_SANITIZE_STRING, $opcoes_texto)
        ];
    }
}
