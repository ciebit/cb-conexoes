<?php

namespace Ciebit\Conexoes\Configuracoes;

class Configuracao
{
    static private $banco; #string
    static private $senha; #string
    static private $servidor; #string
    static private $tipo; #string
    static private $usuario; #string

    public function definirBanco(string $banco): self
    {
        self::$banco = $banco;
        return $this;
    }

    public function definirSenha(string $senha): self
    {
        self::$senha = $senha;
        return $this;
    }

    public function definirServidor(string $servidor): self
    {
        self::$servidor = $servidor;
        return $this;
    }

    public function definirTipo(string $tipo): self
    {
        self::$tipo = $tipo;
        return $this;
    }

    public function definirUsuario(string $usuario): self
    {
        self::$usuario = $usuario;
        return $this;
    }

    public function obterBanco(): string
    {
        return self::$banco;
    }

    public function obterSenha(): string
    {
        return self::$senha;
    }

    public function obterServidor(): string
    {
        return self::$servidor;
    }

    public function obterTipo(): string
    {
        return self::$tipo;
    }

    public function obterUsuario(): string
    {
        return self::$usuario;
    }
}
