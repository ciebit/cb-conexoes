<?php

namespace Ciebit\Conexoes;

trait SqlParametros
{
    /*
     * Padrão:
     * coluna
     * - valor - valor do filtro
     * - operacao - =, <, > ...
     * - prefixo - arq, pes, mat ...
     * - tipo - STRING, INT ...
    */
    private $parametros;

    private function configParametros(\PDOStatement $Pedido):self
    {
        if (! $this->parametros) {
            return $this;
        }

        foreach ($this->parametros as $chave => $param) {
            $Pedido->bindParam(":{$chave}", $param[0], $param[1]);
        }

        return $this;
    }
}
