<?php

namespace Ciebit\Conexoes;

trait SqlOrdem
{
    private $config_ordem;

    /**
     * Configura a ordenação do dados
     * @param $campo aceita string ou array
     */
    public function configOrdem($campo, string $direcao = 'ASC'):self
    {
        $this->config_ordem = [];

        if ($campo === '*') {
            $this->config_ordem[0] = '*';
        }

        // Caso lista de ornenação
        if (is_array($campo)) {
            $this->config_ordem[0] = $campo;
            return $this;
        } else {
            // Configurando o campo
            $this->config_ordem[0] = filter_var($campo, FILTER_SANITIZE_STRING);
        }

        // Verificando dieração
        $this->config_ordem[1] = $direcao == 'ASC' ? 'ASC' : 'DESC';

        return $this;
    }

    /**
     * Gera o trecho de ordenação de um comando SQL
     */
    public function gerarOrdem()
    {
        $sql = '';

        // Se não houver definição retornar uma string vazia
        if (! $this->config_ordem) {
            return $sql;
        }

        $sql = 'ORDER BY ';

        // Verificando se é uma função
        if ($this->config_ordem[0] === '*') {
            return $sql.= 'RAND() ';
        }

        // Verificando se não é uma lista
        if (! is_array($this->config_ordem[0])) {
            return $sql.= "`{$this->config_ordem[0]}` {$this->config_ordem[1]} ";
        }

        foreach ($this->config_ordem[0] as $col) {
            $sql.= "`{$col[0]}` {$col[1]}, ";
        }

        return substr($sql, 0, -2);
    }
}
