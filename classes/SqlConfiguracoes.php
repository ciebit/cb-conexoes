<?php

namespace Ciebit\Conexoes;

trait SqlConfiguracoes
{
    private $cfg_ordem;
    private $cfg_limite;

    /**
     * Configura o limite de linhas obtidas
     */
    public function cfgLimite(int $ini = null, int $fim = null):self
    {
        $this->cfg_limite = [$ini, $fim];
        return $this;
    }

    /**
    * Configura a ordenação do dados
    */
    public function cfgOrdem($campo, string $direcao = 'ASC'):self
    {
        $this->cfg_ordem = [];

        if ($campo === 'cbALEATORIO') {
            $this->cfg_ordem[0] = 'cbALEATORIO';
        }

        // Caso lista de ornenação
        if (is_array($campo)) {
            $this->cfg_ordem[0] = $campo;
            return $this;
        } else {
            // Configurando o campo
            $this->cfg_ordem[0] = filter_var($campo, FILTER_SANITIZE_STRING);
        }

        // Verificando dieração
        $this->cfg_ordem[1] = $direcao == 'ASC' ?? 'DESC';

        return $this;
    }

    /**
    * Gera o trecho de limite de um comando SQL
    */
    public function sqlLimite()
    {
        // Verificando filtro por quantidade
        if (! isset($this->cfg_limite[1])) {
            return '';
        }

        return "LIMIT {$this->cfg_limite[0]}, {$this->cfg_limite[1]} ";
    }

    /**
    * Gera o trecho de ordenação de um comando SQL
    */
    public function sqlOrdem()
    {
        $sql = '';

        // Se não houver definição retornar uma string vazia
        if (! $this->cfg_ordem) {
            return $sql;
        }

        $sql = 'ORDER BY ';

        // Verificando se é uma função
        if ($this->cfg_ordem[0] === 'cbALEATORIO') {
            return $sql.= 'RAND() ';
        }

        // Verificando se não é uma lista
        if (! is_array($this->cfg_ordem[0])) {
            return $sql.= "`{$this->cfg_ordem[0]}` {$this->cfg_ordem[1]} ";
        }

        foreach ($this->cfg_ordem[0] as $col) {
            $sql.= "`{$col[0]}` {$col[1]}, ";
        }

        return substr($sql, 0, -2);
    }
}
