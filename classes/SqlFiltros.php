<?php
namespace Ciebit\Conexoes;

use PDO;

trait SqlFiltros
{
    /*
     * Padrão:
     * coluna
     * - valor - valor do filtro
     * - operacao - =, <, > ...
     * - prefixo - arq, pes, mat ...
     * - tipo - STRING, INT ...
    */
    private $filtros;
    private $filtros_grupo;

    private function definirFiltro(string $campo, $valor, string $operacao, int $tipo)
    {
        $prefixo = '';

        if (strpos($campo, '.')) {
            $prefixo = strstr($campo, '.', true);
            $campo = substr(strstr($campo, '.'), 1);
        }

        $this->filtros[$campo] = [$valor, $operacao, $prefixo, $tipo];
    }

    private function gerarFiltros():string
    {
        $simples = $this->gerarFiltrosSimples();
        $grupo = $this->gerarFiltrosGrupos();

        if (! $simples && ! $grupo) {
            return '';
        } elseif ($simples && $grupo) {
            $sql = $simples.' AND '.$grupo;
        } else {
            $sql = $simples.$grupo;
        }

        return 'WHERE '.$sql;
    }

    private function gerarFiltrosSimples():string
    {
        if (! $this->filtros) {
            return '';
        }

        $sql = '';

        foreach ($this->filtros as $coluna => $param) {
            $valor = $param[0];
            $operacao = $param[1];
            $prefixo = $param[2];
            $tipo = $param[3];

            if ($sql) {
                $sql.= ' AND ';
            }

            if (is_array($valor)) {
                $operacao = $operacao == '=' ? 'IN' : 'NOT IN';

                $parametros = '';

                foreach ($valor as $chave => $value) {
                    $col = $coluna.$chave;
                    $parametros.= ":{$col},";
                    $this->parametros[$col] = [$value, $tipo];
                }
                $parametros = substr($parametros, 0, -1);

                $sql.= "`{$prefixo}`.`{$coluna}` {$operacao}({$parametros}) ";
            } else {
                $valor = $valor;
                $sql.= "`{$prefixo}`.`{$coluna}` {$operacao} :{$coluna} ";
                $this->parametros[$coluna] = [$valor, $tipo];
            }
        }

        return $sql;
    }

    private function gerarFiltrosGrupos():string
    {
        if (! $this->filtros_grupo) {
            return '';
        }

        $sql = '';

        foreach ($this->filtros_grupo as $indice => $codigo) {
            $sql.= $sql ? ' AND ' : '';
            $sql.= $codigo;
        }

        return $sql;
    }
}
