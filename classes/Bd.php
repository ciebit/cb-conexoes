<?php

namespace Ciebit\Conexoes;

use Ciebit\Conexoes\Configuracoes\Persistencias\ArquivoIni as ConfigPersistencia;
use Exception;
use PDO;

class Bd
{
    private $Conexao;
    private $conexao_estrangeira = false;
    private $transicao;

    /**
     * Fechando transições e conexão
     */
    public function __destruct()
    {
        // Verificando se existe uma transição pendente
        if ($this->transicao) {
            $this->transicao = 0;
            $this->transicaoEncerrar(false);
        }

        // Verificando se existe uma conexão para fechar
        if(! $this->conexao_estrangeira) {
            $this->Conexao =  null;
        }
    }

    /**
     * Realiza uma conexão
     */
    public function conectar(PDO $Cx = null)
    {
        // Para cassos onde uma conexão já estabelecida será utilizada
        // útil para transições no bd e poupar conexões
        if ($Cx) {
            $this->conexao_extrangeira = true;
            return $this->Conexao = $Cx;
        }

        // Se já houver uma conexão estabelecida retorna-lá
        if ($this->Conexao) {
            return $this->Conexao;
        }

        $Configuracoes = (new ConfigPersistencia)->obter();

        try {
            $bd_tipo = $Configuracoes->obterTipo();
            $bd_banco = $Configuracoes->obterBanco();
            $bd_servidor  = $Configuracoes->obterServidor();
            $banco = "{$bd_tipo}:dbname={$bd_banco};host={$bd_servidor};charset=utf8";
            $this->Conexao = new PDO($banco, $Configuracoes->obterUsuario(), $Configuracoes->obterSenha());
        } catch (Exception $e) {
            return false;
        }

        $this->conexao_extrangeira = false;

        // Configurações da conexão
        $this->Conexao->exec("SET TIME_ZONE='-3:00'");

        return $this->Conexao;
    }

    /**
     * Gerencia abertura e fechamento de transições
     */
    public function transicaoIniciar():self
    {
        // Caso solicite o método pela primeira vez
        // cria uma transição
        if (! $this->transicao) {
            $this->Conexao->autocommit( false );
        }

        $this->transicao++;
        return $this;
    }

    /**
     * Gerencia abertura e fechamento de transições
     */
    public function transicaoEncerrar(bool $situacao):self
    {
        $this->transicao--;

        // Se não for o último fechamento ignorar
        if ($this->transicao) {
            return $this;
        }

        // Verificando qual ação tomar
        if ($situacao) {
            $this->Conexao->commit();
        } else {
            $this->Conexao->rollback();
        }

        // Cancelando auto commit
        $this->Conexao->autocommit(true);

        return $this;
    }
}
