# cb Conexões #

Ferramenta base para conexões ao banco de dados.

## Instalação ##

### Composer ###

No arquivo "composer.json" realize as seguintes adições:

```
#!json
{
    "require": {
        "ciebit/cb-conexoes": "next",
    },
    "repositories": [
        {
            "type": "composer",
            "url": "https://composer.ciebit.com.br"
        }
    ]
}
```

### Criando arquivo de configuração ###

No terminal execute o seguinte comando:

```
#!shell
composer exec cb-conexoes-iniciar
```

Um arquivo de configuração será criado no diretório atual.
